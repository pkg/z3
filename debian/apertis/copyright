Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: Microsoft Corporation </AssemblyCopyright>
License: Expat

Files: debian/*
Copyright: 2016-2021, Fabian Wolff <fabi.wolff@arcor.de>
 2011, Michael Tautschnig <mt@debian.org>
License: GPL-2+

Files: checking/* examples/SMT-LIB2/* examples/java/* examples/python/complex/* examples/python/hamiltonian/* examples/python/mus/* examples/tptp/* src/* src/api/dotnet/Properties/* src/muz/spacer/*
Copyright: 2006-2021 Microsoft Corporation
 2006, 2010, 2017-2019 Arie Gurfinkel
 2017-2018 Saint-Petersburg State University
 2017 Matteo Marescotti
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the ""Software""), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: debian/tests/include-z3-test
Copyright: and z3++.h (C++) header files
License: GPL-2+

Files: debian/z3.1
Copyright: 2006-2014, Microsoft Corp.
License: GPL-2+

Files: examples/tptp/tptp5.tab.c
 examples/tptp/tptp5.tab.h
Copyright: 1984, 1989, 1990, 2000-2006, 2009, 2010, Free Software
License: GPL-3+ with Bison-2.2 exception

Files: src/api/js/*
Copyright: no-info-found
License: Expat

Files: src/api/js/package-lock.json
Copyright: no-info-found
License: Expat or ISC

Files: contrib/qprofdiff/main.cpp doc/mk_api_doc.py doc/mk_params_doc.py doc/mk_tactic_doc.py examples/c++/example.cpp examples/c/test_capi.c examples/dotnet/Program.cs examples/maxsat/maxsat.c examples/ml/ml_example.ml examples/python/all_interval_series.py examples/python/bounded examples/python/example.py examples/python/socrates.py examples/python/union_sort.py examples/python/visitor.py model scripts/mk_copyright.py scripts/mk_exception.py scripts/mk_make.py scripts/mk_nuget_task.py scripts/mk_project.py scripts/mk_unix_dist.py scripts/mk_unix_dist_cmake.py scripts/mk_util.py scripts/mk_win_dist.py scripts/mk_win_dist_cmake.py scripts/trackall.sh scripts/update_api.py scripts/update_header_guards.py scripts/update_include.py src/api/api_qe.cpp src/api/dotnet/ArithExpr.cs src/api/dotnet/ArrayExpr.cs src/api/dotnet/BitVecExpr.cs src/api/dotnet/BoolExpr.cs src/api/dotnet/DatatypeExpr.cs src/api/dotnet/Expr.cs src/api/dotnet/FiniteDomainExpr.cs src/api/dotnet/FiniteDomainNum.cs src/api/dotnet/ReExpr.cs src/api/dotnet/RealExpr.cs src/api/dotnet/SeqExpr.cs src/api/z3_spacer.h src/ast/proofs/proof_utils.cpp src/ast/recfun_decl_plugin.cpp src/ast/recfun_decl_plugin.h src/ast/rewriter/factor_equivs.cpp src/ast/rewriter/factor_equivs.h src/muz/spacer/spacer_arith_generalizers.cpp src/muz/spacer/spacer_callback.cpp src/muz/spacer/spacer_callback.h src/muz/spacer/spacer_conjecture.cpp src/muz/spacer/spacer_context.cpp src/muz/spacer/spacer_context.h src/muz/spacer/spacer_dl_interface.cpp src/muz/spacer/spacer_dl_interface.h src/muz/spacer/spacer_farkas_learner.cpp src/muz/spacer/spacer_farkas_learner.h src/muz/spacer/spacer_generalizers.cpp src/muz/spacer/spacer_generalizers.h src/muz/spacer/spacer_manager.cpp src/muz/spacer/spacer_manager.h src/muz/spacer/spacer_mev_array.cpp src/muz/spacer/spacer_mev_array.h src/muz/spacer/spacer_qe_project.cpp src/muz/spacer/spacer_quant_generalizer.cpp src/muz/spacer/spacer_sem_matcher.cpp src/muz/spacer/spacer_sem_matcher.h src/muz/spacer/spacer_sym_mux.cpp src/muz/spacer/spacer_sym_mux.h src/muz/spacer/spacer_util.cpp src/muz/spacer/spacer_util.h src/muz/transforms/dl_mk_elim_term_ite.cpp src/muz/transforms/dl_mk_elim_term_ite.h src/muz/transforms/dl_mk_synchronize.cpp src/muz/transforms/dl_mk_synchronize.h src/parsers/smt2/marshal.cpp src/parsers/smt2/marshal.h src/qe/mbp/mbp_term_graph.cpp src/qe/mbp/mbp_term_graph.h src/smt/theory_recfun.cpp src/test/main.cpp src/util/min_cut.cpp src/util/min_cut.h
Copyright: 2006-2021 Microsoft Corporation
 2006, 2010, 2017-2019 Arie Gurfinkel
 2017-2018 Saint-Petersburg State University
 2017 Matteo Marescotti
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the ""Software""), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
